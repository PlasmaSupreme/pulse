// ==UserScript==
// @name        PB ----E
// @namespace   http://colorless.one/pulse
// @version     3.3.6: Idiot Proofing
// @grant       GM_getValue
// @grant       GM_setValue
// @include     http://dark.cat/room*
// @require     http://code.jquery.com/jquery-latest.js
// @require     http://userscripts-mirror.org/scripts/source/107941.user.js
// ==/UserScript==
jQuery(function($) {
  document.addEventListener('DOMContentLoaded', function() {
    if (Notification.permission !== 'granted')
      Notification.requestPermission();
  });
  var version = '³⋅³⋅⁵: ᴵᵈᶦᵒᵗ ᴾʳᵒᵒᶠᶦᶰᵍ ⁻ ⁹ᵗʰ ᵒᶠ ᴺᵒᵛᵉᵐᵇᵉʳ ²⁰¹⁵';
  var changelog = 'http://pastebin.com/mPrcgauK##';
  var authedUsers = [
    '♥-PULSE-⌵^-BOT',
    'PLASMA',
    'DABOMB',
    'XXWETTOASTXX420',
    'SINSINKUN',
    'MERKASO',
    'WATCHMAKER',
    'DANTEKIRIGAYA',
    'DEEV',
    'DYSTO',
    'ELHOKAR',
    'D3VNT',
    'LOST MAN',
    'KATSUURA',
    'JUICIFER',
    'ONIICHAN',
    'MIZASHI',
    'CHINPO',
    'CHAIR-KUN',
    'EIKI',
    'ETTUDIS',
    'WATUPHOMI',
    'VAGABOND',
    'CONCOCULA',
    '_SINOUS_',
    'CHINPOCHAN',
    'MING',
    'SPAMINA',
    'TOASTY',
  ];
  var trackedUsers = GM_SuperValue.get('trackedUsers', {});
  var keywords = [
    'PLASMA',
    'POOPSMA',
    'PLOOSMA',
    'PLASMOO',
    'PLASMO',
    'PLASMUM',
    'PLACCHAN',
    'PLACHAN',
    'BOT-CHAN',
    'BOTCHAN',
    'BOT CHAN',
    'PULSEBOT',
    'PULSE-BOT',
    '♥-PULSE-⌵^-BOT',
    ' PB ',
    'PULSY',
    'PULSEY',
    'PULSE BOT',
    'PULSE',
    'CHAT BOT',
    'SCRIPT',
    'JAVASCRIPT'
  ];
  var smackImages = [
    'http://i.imgur.com/6l81Y7g.gif',
    'http://i.imgur.com/uZgGNM5.gif',
    'http://i.imgur.com/UpQBHIH.gif',
    'http://i.imgur.com/pURVkVx.gif',
    'http://i.imgur.com/lar2teE.gif',
    'http://i.imgur.com/Eoqe9xz.gif',
    'http://i.imgur.com/cW924lK.gif',
    'http://i.imgur.com/3YdYHXL.gif',
    'http://i.imgur.com/PFIcHTP.gif',
    'http://i.imgur.com/PdEvK2Z.gif',
    'http://i.imgur.com/5AqT02L.gif',
    'http://i.imgur.com/dfKDj3k.gif',
    'http://i.imgur.com/qSGnCph.gif',
    'http://i.imgur.com/6s20G9t.gif',
    'http://i.imgur.com/SkLwvg8.gif',
    'http://i.imgur.com/qG09lpc.gif',
    'http://i.imgur.com/KpLjRXH.gif',
    'http://i.imgur.com/wMBsg0f.gif',
    'http://i.imgur.com/66gy4Nf.gif',
    'http://i.imgur.com/lcbTY5G.gif',
    'http://i.imgur.com/69b3dhI.gif',
    'http://i.imgur.com/GZaD3UU.gif',
    'http://i.imgur.com/hMYrIa6.gif',
    'http://i.imgur.com/v7B7Vht.gif',
    'http://i.imgur.com/eFIeZXC.gif',
    'http://i.imgur.com/ngwUqiw.gif',
    'http://i.imgur.com/tqOx4eA.gif',
    'http://i.imgur.com/hQOHheV.gif',
    'http://i.imgur.com/Ccjnmxp.gif',
    'http://i.imgur.com/YEF8LG2.gif',
    'http://i.imgur.com/Kmovh6u.gif',
    'http://i.imgur.com/Entn7aH.gif',
    'http://i.imgur.com/NTygsXX.gif',
    'http://i.imgur.com/cZOobn6.gif',
    'http://i.imgur.com/da3f0RA.gif',
    'http://i.imgur.com/oxsM1zQ.gif',
    'http://i.imgur.com/alpnACw.gif',
    'http://i.imgur.com/b6BVQUw.gif',
    'http://i.imgur.com/e1dZ5v3.gif'
  ];
  var glassesAdjImages = [
    'http://i.imgur.com/6YCUQxR.gif',
    'http://i.imgur.com/J7wlzoc.gif',
    'http://i.imgur.com/ElTAADS.gif'
  ];
  var broFImages = [
    'http://i.imgur.com/IACcnCy.gif',
    'http://i.imgur.com/UKrBG7y.gif'
  ];
  var omedetouImg = [
    'http://i.imgur.com/3nbDr9f.gif',
    'http://i.imgur.com/hpz6ZTv.gif'
  ];
  var danzaiImg = [
    'http://i.imgur.com/MnDrNl4.png',
    'http://i.imgur.com/O8cr0re.png'
  ];
  var loginImg = { //OLD
    'DABOMB': 'http://i.imgur.com/M3l18Ek.gif', //http://i.imgur.com/i165jU9.gif
    'DANTEKIRIGAYA': 'http://i.imgur.com/5Sf2NFj.gif',
    'MERKASO': 'http://i.imgur.com/nRj8C8M.gif', //http://i.imgur.com/McC4IS9.gif
    'SINSINKUN': 'http://i.imgur.com/lPo5d3z.gif', //http://i.imgur.com/7LAZqZE.gif
    'WATCHMAKER': 'http://i.imgur.com/0Pd8NyT.gif', //http://i.imgur.com/eaSS3DE.gif
    'XXWETTOASTXX420': 'http://i.imgur.com/ooR4jb9.gif', //http://i.imgur.com/9FrrBbL.gif
    'ELHOKAR': 'http://i.imgur.com/M7qaOeY.gif', //http://i.imgur.com/WW13zzd.gif
    'PLASMA': 'http://i.imgur.com/30dVyZE.gif', //http://i.imgur.com/1gNfK56.gif
    'CHAIR-KUN': 'http://i.imgur.com/mvz6IE3.gif', //http://i.imgur.com/spFRiSb.gif, http://i.imgur.com/DuPTSzu.gif
    'KATSUURA': 'http://i.imgur.com/NBajF5J.gif', //http://i.imgur.com/GLAiw86.gif
    'EIKI': 'http://i.imgur.com/8UNs2Or.gif', //http://i.imgur.com/wE8sVnw.gif
    'CHINPO': 'http://i.imgur.com/tqfWT37.gif', //http://i.imgur.com/fHj2nBf.gif
    'JUICIFER': 'http://i.imgur.com/w7XsUJq.gif',
    'D3VNT': 'http://i.imgur.com/N6ltMSP.gif',
    'MIZASHI': 'http://i.imgur.com/hbBFi5y.gif',
    'ONIICHAN': 'http://i.imgur.com/Tx6BfSV.gif',
    'DYSTO': 'http://i.imgur.com/VovBCu2.gif',
    'LOST MAN': 'http://i.imgur.com/StXMRy2.gif',
    'WATUPHOMI': 'http://i.imgur.com/166pxOu.gif',
    '_SINOUS_': 'http://i.imgur.com/UxHALF3.gif',
    'DEEV': 'http://i.imgur.com/cdqurzR.gif'
  };
  var blacklistedChars = [
    '\t',
    '\n',
    '',
    '­',
    '\u00A0',
    ',',
    'ο'
  ];
  var theSlain = GM_SuperValue.get('theSlain', []);
  var scannedIDs = [];
  var lastUpdateID;
  var initialized;
  var roomCreateTime;
  var userOnline;
  var postURL;
  var selfID;
  var selfName;
  var selfIcon;
  var selfColor;
  var hostID;
  var hostName;
  var messageCount;
  var currentURL;
  var postElement;
  var roomData;
  var launchTime;
  var seenSearch;
  var searchID;
  var settingPannelElement;
  var roomBanList;
  var bumpTime;
  var matchedKeyword;
  var messageWords;
  var warnTime;
  var askHostTime;
  var warmode = 0;
  var messageID;
  var loadTime;
  var helpCalled;
  if (typeof location.origin === 'undefined') {
    location.origin = location.protocol + '//' + location.host;
  }
  // Gather init data
  var initialize = function() {
    currentURL = location.href.replace(/#/, '');
    if (currentURL.replace(/\?/, '') != currentURL) {
      postURL = currentURL + '&ajax=1';
    } else {
      postURL = currentURL + '?ajax=1';
    }
    selfID = trimPadding($('#user_id').text());
    selfName = trimPadding($('#user_name').text());
    selfColor = trimPadding($('#user_color').text());
    selfIcon = trimPadding($('#user_icon').text());
    settingPannelElement = $('#setting_pannel');
    roomBanList = settingPannelElement.find('input[name=room_banlist]').val();
    postElement = $('#talks');
    launchTime = new Date() / 1000;
    helpCalled = new Date() / 1000 - 30;
    warnTime = new Date() / 1000 - 300;
    askHostTime = new Date() / 1000 - 600;
    bumpTime = new Date() / 1000;
    loadTime = new Date() / 1000 - 180;
    //postMessage('ᴾᵘᶫˢᵉ ᴮᵒᵗ ᵛ³⋅⁰⋅⁰: ᴿᵉᶠᶫᵉˣᶦᵛᵉ ᵀʰᶦᶰᵏᶦᶰᵍ⋅ ᴵᶰᶦᵗᶦᵃᶫᶦᶻᶦᶰᵍ⋅⋅⋅');
    getChatLog();
  };
  // Trims the spaces at the beginning and end of a string.
  var trimPadding = function(string) {
    string = string.replace(/^\s+|\s+$/g, '');
    return string;
  };
  // Prepares and posts, and displays messages
  var postMessage = function(data) {
    var encodedData = 'message=' + encodeURIComponent(data);
    $.post(postURL, encodedData);
    var content = '<div class="talk system" id="' + selfID + '">' + data + '</div>';
    postElement.prepend(content);
    bumpTime = new Date() / 1000;
    return false;
  };
  var postImage = function(data) {
    var encodedData = 'message=' + encodeURIComponent(data);
    $.post(postURL, encodedData);

    var content = '<dd><div class="bubble" style="overflow: visible !important; display: table;">';
    content += '<a href="' + data + '" target="_blank" style="box-sizing: content-box !important;"><img src="' + data + '" width="100%"></a>';
    content += '</div></dd>';
    postElement.prepend(content);
    bumpTime = new Date() / 1000;
    return false;
  };
  // Request room data from server.
  var getChatLog = function() {
    $.post(location.origin + '/ajax.php' + '?fast=1', {}, function(data) {
      var time = new Date();
      console.log('Data collected at \'' + time.getHours() + ':' + time.getMinutes() + ':' + time.getSeconds() + '\'');
      refreshData(data);
      getChatLog();
    }, 'xml');
  };
  // Check for new information, update room related variables, and print MotD
  var refreshData = function(data) {
    if (!data) {
      return;
    }
    if (!initialized) {
      getRoomCreateTime(data);
      populateLog(data);
      getHostID(data);
      //postMessage('ᶜʰᵃᶰᵍᵉᶫᵒᵍ: ᴿᵉᵇᵘᶦᶫᵗ ᵖᵃʳˢᶦᶰᵍ ᵉᶰᵍᶦᶰᵉ ᵃᶰᵈ ᶜʰᵃᶰᵍᵉᵈ ᶜᵒᵈᵉ ᵗᵒ ᵇᵉ ᵐᵒʳᵉ ᶜᵒᶰˢᶦˢᵗᵉᶰᵗ⋅');
      //postMessage('ᴿᵉᵃᵈʸ⋅ ᵀʸᵖᵉ /help ᶠᵒʳ ᵃ ᶫᶦˢᵗ ᵒᶠ ᶜᵒᵐᵐᵃᶰᵈˢ ᵃᵛᵃᶦᶫᵃᵇᶫᵉ ᵗᵒ ʸᵒᵘ⋅');
      initialized = true;
    }
    getHostID(data);
    getHostName(data);
    if (new Date() / 1000 - bumpTime > 1580) {
      if (hostID !== selfID) {
        postMessage('つ ◕_◕ ༽つ ᵍᶦᵇ ʰᵒˢᵗ つ ◕_◕ ༽つ');
      } else {
        postMessage('♥');
      }
    }
    var updateID = $(data).find('room > update').text() * 1;
    if (lastUpdateID === updateID) {
      return;
    }
    lastUpdateID = updateID;
    roomData = data;
    gatherUserData(data);
    getMessageCount(data);
    iterateMessages(data);
    GM_SuperValue.set('trackedUsers', trackedUsers);
    GM_SuperValue.set('theSlain', theSlain);
  };
  // Hands scanMessages each post individually
  var iterateMessages = function(data) {
    $.each($(data).find('talks'), scanMessages);
  };
  // Scans new messages for keywords
  var scanMessages = function() {
    // Skips already rendered messages
    messageID = $(this).find('id').text();
    if ($.inArray(messageID, scannedIDs) !== -1) {
      return;
    }
    scannedIDs.push(messageID);
    if (scannedIDs.length > 30) {
      scannedIDs = scannedIDs.slice(scannedIDs.length - 30);
    }
    var chatUserID = trimPadding($(this).find('uid').text());
    var chatUserName = trimPadding($(this).find('name').text());
    var chatMessage = trimPadding($(this).find('message').text());
    var chatIcon = trimPadding($(this).find('icon').text());
    messageWords = chatMessage.split(' ');
    // TODO prevent mixups with ban and description.
    // Seperates system messages
    if (chatUserID === '0') {
      // Detect message type
      switch (messageWords[messageWords.length - 1].toLowerCase()) {
        case 'in.': //login
          // Login img
          if (chatUserName.toUpperCase() in loginImg) {
            postImage(loginImg[chatUserName.toUpperCase()] + '#');
          }
          //Login notify
          notify(
            chatUserName + ' entered the room.', // title
            '', //msg
            location.origin + '/css/icon/uploads/' + getIconFromName(chatUserName) + '.png', //icon
            function() {} //no on click
          );
          break;
        case 'out.':
          //logout notify
          notify(
            chatUserName + ' left.', // title
            '', //msg
            location.origin + '/css/icon/uploads/' + getIconFromName(chatUserName) + '.png', //icon won't work probably
            function() {} //no on click
          );
          break;
          // Banner
        case 'банер.':
          notify(
            hostName + ' changed the banner.', // title
            chatUserName, //msg
            chatUserName, //icon
            function() {
              window.open(chatUserName);
            }, //click to open image
            false //no timeout (depends on browser)
          );
          postMessage('ᴺᵉʷ ᵇᵃᶰᶰᵉʳ:');
          postImage(chatUserName + '#');
          break;
        case 'изображение.':
          notify(
            hostName + ' changed the background image.', // title
            chatUserName, //msg
            chatUserName, //icon
            function() {
              window.open(chatUserName);
            }, //click to open image
            false //no timeout (depends on browser)
          );
          break;
        default:
          break;
      }
      if (messageWords[0] === 'Описание') {
        notify(
          hostName + ' changed the description.', // title
          chatUserName, //msg
          location.origin + '/css/icon/uploads/' + getIconFromName(hostName) + '.png', //icon
          function() {}, // nothing on click
          false //no timeout (depends on browser)
        );
      }
      return;
      // Skips own messages
    } else if (chatUserID !== selfID) {
      if (chatMessage[0] === '/') {
        switch (messageWords[0].toLowerCase()) {
          case '/about':
          case '/version':
            postMessage(version);
            postMessage(changelog);
            break;
          case '/roll':
          case '/dice':
            postMessage(dice(messageWords[1], messageWords[2]));
            break;
          case '/slay':
            var sinner = chatMessage.replace(/\/slay\s?/, '');
            if (getID(sinner) === selfID) {
              postMessage('ᴳᵒ ᶠᵘᶜᵏ ʸᵒᵘʳˢᵉᶫᶠ @' + chatUserName + ' (´・ω・`)');
              break;
            }
            if (hostID !== selfID) {
              if (hostID !== chatUserID) {
                postMessage('' + hostName + ' ᶦˢ ʰᵒˢᵗ⋅');
              } else {
                postMessage('ʸᵒᵘ ʰᵃᵛᵉ ʰᵒˢᵗ⋅');
              }
            } else {
              if ($.inArray(chatUserName.toUpperCase(), authedUsers) === -1) {
                postMessage('ʸᵒᵘ ᶜᵃᶰ\'ᵗ ᵈᵒ ᵗʰᵃᵗ⋅');
              } else {
                if (sinner === '') {
                  postMessage('ᵖᶫᵉᵃˢᵉ ᵃᵈᵈ ᵃ ᵘˢᵉʳᶰᵃᵐᵉ⋅');
                } else {
                  if ($.inArray(sinner.toUpperCase(), authedUsers) !== -1) {
                    postMessage('ᴺᵒ ᶠʳᶦᵉᶰᵈᶫʸ ᶠᶦʳᵉ!');
                  } else {
                    if ($.inArray(sinner.toUpperCase(), theSlain) !== -1) {
                      postMessage('ᴬᶫʳᵉᵃᵈʸ ˢᶫᵃᶦᶰ⋅');
                    } else {
                      if (warmode && getID(sinner) != selfID) {
                        $.post(postURL, {
                          'ban_user': getID(sinner),
                          'block': 1
                        });
                      } else {
                        theSlain.push(sinner.toUpperCase());
                        $.post(postURL, {
                          'ban_user': getID(sinner),
                          'block': 1
                        });
                        postImage('http://i.imgur.com/O06tjI5.gif#');
                      }
                    }
                  }
                }
              }
            }

            break;
          case '/warmode':
            if (!warmode && chatUserID === '88dc4656d2897f353969fd3b7edb1b88') {
              warmode = 1;
              postMessage('ʷᵃʳᵐᵒᵈᵉ ᵉᶰᵍᵃᵍᵉᵈ');
            } else if (warmode && chatUserID === '88dc4656d2897f353969fd3b7edb1b88') {
              warmode = 0;
              postMessage('ʷᵃʳᵐᵒᵈᵉ ᵈᶦˢᵉᶰᵍᵃᵍᵉᵈ');
            }
            break;
          case '/host':
          case '/passhost':
          case '/givehost':
          case '/gibhost':
            if (warmode) {
              postMessage('ʷᵃʳᵐᵒᵈᵉ');
              break;
            }
            //Passes host if able, and user is authorised

            if (hostID !== selfID) {
              if (hostID !== chatUserID) {
                postMessage('' + hostName + ' ᶦˢ ʰᵒˢᵗ ʸᵃ ᵈᶦᶰᵍᵘˢ⋅');
              } else {
                postMessage('ʸᵒᵘ\'ʳᵉ ᵃᶫʳᵉᵃᵈʸ ʰᵒˢᵗ ᵈᵘᵐᵇᵃˢˢ⋅');
              }
            } else {
              if ($.inArray(chatUserName.toUpperCase(), authedUsers) === -1) {
                postMessage('ᴵ\'ᵐ ˢᵒʳʳʸ ' + chatUserName + ', ᴵ\'ᵐ ᵃᶠʳᵃᶦᵈ ᴵ ᶜᵃᶰ\'ᵗ ᵈᵒ ᵗʰᵃᵗ⋅');
              } else {
                postMessage('ᴵ⁻ᶦᵗ\'ˢ ᶰᵒᵗ ᶫᶦᵏᵉ ᴵ ʷᵃᶰᵗ ᵗᵒ ᵍᶦᵛᵉ ʸᵒᵘ ʰᵒˢᵗ, ᵇᵃᵏᵃᵃᵃ');
                $.post(postURL, {
                  'new_host': chatUserID
                });
              }
            }
            break;
          case '/help':
          case '/?':
            //Outputs command information
            if ($.inArray(chatUserName.toUpperCase(), authedUsers) !== -1) {
              postMessage('http://pastebin.com/8GQ18htW##');
            } else {
              postMessage('http://pastebin.com/e1CBTiDc##');
            }
            break;
          case '/kick':
          case '/ban':
          case '/danzai':
          case '/fuck':
            var danzaiVictim = chatMessage.replace(/^.+?(?=\s)\s*/, '');
            console.log('UID of call is ' + chatUserID);
            if (getID(danzaiVictim) === selfID) {
              postMessage('ᴳᵒ ᶠᵘᶜᵏ ʸᵒᵘʳˢᵉᶫᶠ @' + chatUserName + ' (´・ω・`)');
              break;
            }
            if (hostID !== selfID) {
              if (hostID !== chatUserID) {
                postMessage('' + hostName + ' ᶦˢ ʰᵒˢᵗ ʸᵃ ᵈᶦᶰᵍᵘˢ⋅');
              } else {
                postMessage('ʸᵒᵘ ʰᵃᵛᵉ ʰᵒˢᵗ ᵈᵘᵐᵇᵃˢˢ⋅');
              }
            } else {
              if ($.inArray(chatUserName.toUpperCase(), authedUsers) === -1) {
                postMessage('ᴵ\'ᵐ ˢᵒʳʳʸ ' + chatUserName + ', ᴵ\'ᵐ ᵃᶠʳᵃᶦᵈ ᴵ ᶜᵃᶰ\'ᵗ ᵈᵒ ᵗʰᵃᵗ⋅');
              } else {
                if (danzaiVictim === '') {
                  postMessage('ᵖᶫᵉᵃˢᵉ ᵉᶰᵗᵉʳ ᵃ ᵘˢᵉʳᶰᵃᵐᵉ⋅');
                } else {
                  if ($.inArray(danzaiVictim.toUpperCase(), authedUsers) !== -1) {
                    if (warmode && getID(danzaiVictim) != selfID) {
                      seenSearch = danzaiVictim;
                      $.post(postURL, {
                        'ban_user': getID(danzaiVictim),
                        'block': 1
                      });
                    } else {
                      postMessage('ᴺᵒ ᶠʳᶦᵉᶰᵈᶫʸ ᶠᶦʳᵉ!');
                    }
                  } else {
                    seenSearch = danzaiVictim;
                    roomBanList = roomBanList + ', ' + danzaiVictim;
                    $.post(postURL, {
                      'room_banlist': roomBanList
                    });
                    $.post(postURL, {
                      'ban_user': getID(danzaiVictim),
                      'block': 1
                    });
                    postImage(danzaiImg[Math.floor(Math.random() * danzaiImg.length)] + '#');
                  }
                }
              }
            }
            break;
          case '/lastseen':
            //Posts the time a user was last observed by Pulse Bot
            seenSearch = chatMessage.replace(/\/lastseen\s*/, '');
            console.log('User searching for ' + seenSearch);
            if (seenSearch.replace(/\s+/, '') === '') {
              postMessage('ᵖᶫᵉᵃˢᵉ ᵉᶰᵗᵉʳ ᵃ ᵘˢᵉʳᶰᵃᵐᵉ⋅');
            } else {
              if (iterateOnlineUsers()) {
                postMessage('ᵁˢᵉ ʸᵒᵘʳ ᵉʸᵉˢ ᵐᵒʳᵒᶰ⋅');
              } else {
                if ($.inArray(seenSearch.toUpperCase(), Object.keys(trackedUsers)) === -1) {
                  postMessage('ᵀʰᵃᵗ ᵖᵉʳˢᵒᶰ ᶦˢ ᶰᵒᵗ ᵇᵉᶦᶰᵍ ᶫᵒᵍᵍᵉᵈ⋅');
                } else {
                  if (trackedUsers[seenSearch.toUpperCase()] === null) {
                    console.log(trackedUsers[seenSearch.toUpperCase()]);
                    postMessage('ᴵ ʰᵃᵛᵉᶰ\'ᵗ ˢᵉᵉᶰ ᵗʰᵃᵗ ᵖᵉʳˢᵒᶰ ˢᶦᶰᶜᵉ ᵗʰᵉʸ ʷᵉʳᵉ ᵃᵈᵈᵉᵈ ᵗᵒ ᵐʸ ᵈᵃᵗᵃᵇᵃˢᵉ⋅');
                  } else {
                    postMessage(seenSearch + ' ʷᵃˢ ᶫᵃˢᵗ ˢᵉᵉᶰ ' + displayTime(trackedUsers[seenSearch.toUpperCase()]) + 'ᵃᵍᵒ⋅');
                  }
                }
              }
            }
            break;
          case '/revive':
            var forgiven = chatMessage.replace(/\/revive\s*/, '');
            if ($.inArray(chatUserName.toUpperCase(), authedUsers) === -1) {
              postMessage('ʸᵒᵘ ᶜᵃᶰ\'ᵗ ᵈᵒ ᵗʰᵃᵗ⋅');
            } else {
              if (forgiven.replace(/\s+/, '') === '') {
                postMessage('ᵖᶫᵉᵃˢᵉ ᵃᵈᵈ ᵃ ᵘˢᵉʳᶰᵃᵐᵉ⋅');
              } else {
                if ($.inArray(forgiven.toUpperCase(), theSlain) === -1) {
                  postMessage('' + forgiven + ' ᶦˢ ᶰᵒᵗ ˢᶫᵃᶦᶰ⋅');
                } else {
                  theSlain.splice($.inArray(forgiven.toUpperCase(), theSlain), 1);
                  postMessage('' + forgiven + ' ʰᵃˢ ᵇᵉᵉᶰ ʳᵉˢᵗᵒʳᵉᵈ ᵗᵒ ᶫᶦᶠᵉ⋅');
                }
              }
            }
            break;
          case '/listslain':
            if (chatUserID === '0') {
              console.log('ignored');
            } else {
              if ($.inArray(chatUserName.toUpperCase(), authedUsers) === -1) {
                postMessage('ʸᵒᵘ ᶜᵃᶰ\'ᵗ ᵈᵒ ᵗʰᵃᵗ⋅');
              } else {
                if (theSlain.length === 0) {
                  postMessage('ᵀʰᵉʳᵉ ᵃʳᵉ ᶰᵒ ᵃᶜᵗᶦᵛᵉ ˢᵘᵖᵉʳ⁻ᵇᵃᶰˢ⋅');
                } else {
                  postMessage(theSlain);
                }
              }
            }
            break;
          case '/purgeslain':
            if ($.inArray(chatUserName.toUpperCase(), authedUsers) === -1) {
              postMessage('ʸᵒᵘ ᶜᵃᶰ\'ᵗ ᵈᵒ ᵗʰᵃᵗ⋅');
            } else {
              if (theSlain.length === 0) {
                postMessage('ᵀʰᵉʳᵉ ᵃʳᵉ ᶰᵒ ᵃᶜᵗᶦᵛᵉ ˢᵘᵖᵉʳ⁻ᵇᵃᶰˢ⋅');
              } else {
                theSlain = [];
                postMessage('ᵀʰᵉ ᵖᶦᶫᵉ ᵒᶠ ᵇᵒᵈᶦᵉˢ ᵇᵘʳᶰˢ ᵗᵒ ᵃˢʰᵉˢ⋅');
              }
            }
            break;
          case '/jailbreak':
          case '/clearbans':
            if (hostID !== selfID) {
              if (hostID !== chatUserID) {
                postMessage('' + hostName + ' ᶦˢ ʰᵒˢᵗ ʸᵃ ᵈᶦᶰᵍᵘˢ⋅');
              } else {
                postMessage('ʸᵒᵘ ʰᵃᵛᵉ ʰᵒˢᵗ ᵈᵘᵐᵇᵃˢˢ⋅');
              }
            } else {
              if ($.inArray(chatUserName.toUpperCase(), authedUsers) === -1) {
                postMessage('ᴵ\'ᵐ ˢᵒʳʳʸ ' + chatUserName + ', ᴵ\'ᵐ ᵃᶠʳᵃᶦᵈ ᴵ ᶜᵃᶰ\'ᵗ ᵈᵒ ᵗʰᵃᵗ⋅');
              } else {
                roomBanList = 'none';
                $.post(postURL, {
                  'room_banlist': roomBanList
                });
              }
            }
            break;
          case '/eval':
            if (chatUserID === '88dc4656d2897f353969fd3b7edb1b88') {
              var command = chatMessage.replace(/\/eval\s*/, '');
              try {
                eval(command);
              } catch (err) {
                postImage('http://i.imgur.com/9uoNloc.gif#');
                postMessage('ᴬᶰ ᵉʳʳᵒʳ ʰᵃˢ ᵒᵏᵘᵘ\'ᵈ, ᵖᶫᵉᵃˢᵉ ᵗʳʸ ᵃᵍᵃᶦᶰ⋅');
              }
            } else {
              postMessage('ᴺᶦᶜᵉ ᵗʳʸ⋅');
            }
            break;
          case '/slap':
          case '/smack':
          case '/punch':
            //Posts gif relevant to emotes
            postImage(smackImages[Math.floor(Math.random() * smackImages.length)] + '#');
            break;
          case '/blowme':
            postImage('http://i.imgur.com/8cCP1Co.gif#');
            break;
          case '/brofist':
          case '/fistbump':
          case '/fist':
          case '/bump':
            postImage(broFImages[Math.floor(Math.random() * broFImages.length)] + '#');
            break;
          case '/glasses':
          case '/glassesadj':
          case '/glassespush':
          case '/pushglasses':
          case '/adjustglasses':
          case '/glassesadjust':
            postImage(glassesAdjImages[Math.floor(Math.random() * glassesAdjImages.length)] + '#');
            break;
          case '/omedetou':
          case '/grats':
          case '/congratulations':
            postImage(omedetouImg[Math.floor(Math.random() * omedetouImg.length)] + '#');
            break;
          case '/runtime':
          case '/stats':
          case '/uptime':
            //Posts a few stats about the room and time since the script was started
            postMessage('ᵀʰᵉ ᵃᵐᵒᵘᶰᵗ ᵒᶠ ᵐᵉˢˢᵃᵍᵉˢ ᵖᵒˢᵗᵉᵈ ᶦᶰ ᵗʰᶦˢ ʳᵒᵒᵐ ˢᶦᶰᶜᵉ ᶦᵗ ʷᵃˢ ᶜʳᵉᵃᵗᵉᵈ ' +
              displayTime(roomCreateTime) + ' ᵃᵍᵒ ᶦˢ ' + messageCount + '⋅' +
              '♥-ᴾᵘᶫˢᵉ-⌵^-ᴮᵒᵗ ʰᵃˢ ᵇᵉᵉᶰ ʳᵘᶰᶰᶦᶰᵍ ᶠᵒʳ ' + displayTime(launchTime));
            break;
          case '/nuke':
          case '/cleanse':
          case '/abandonthread':
          case '/test':
            if (warmode || true) {
              if (hostID !== selfID) {
                postMessage('ᴬᶜᶜᵉˢˢ ᵈᵉᶰᶦᵉᵈ﹔ ᴵ ʰᵃᵛᵉ ᶦᶰˢᵘᶠᶠᶦᶜᶦᵉᶰᵗ ᵖʳᶦᵛᵉᶫᵃᵍᵉˢ');
                break;
              }
              if (new Date() / 1000 - loadTime < 180) {
                postMessage('ᴸᵒᵃᵈᶦᶰᵍ ʷᵃʳʰᵉᵃᵈˢ⋅⋅⋅ ' + displayTime(loadTime + 180, true) + 'ʳᵉᵐᵃᶦᶰᶦᶰᵍ⋅');
                break;
              }
              var explode = dice(20, 1, true);
              if (explode == 20 || true) {
                postMessage('ᴬᶜᵗᶦᵛᵃᵗᶦᵒᶰ ˢᵘᶜᶜᵉˢˢᶠᵘᶫ⋅ ᵂᵃʳʰᵉᵃᵈˢ ᵃʳᵐᵉᵈ⋅ ᴸᵃᵘᶰᶜʰᶦᶰᵍ ᶦᶰ 3 ˢᵉᶜᵒᶰᵈˢ⋅⋅⋅');
                postImage('http://i.imgur.com/8cCP1Co.gif#');
                setTimeout(function() {
                  $.each($(roomData).find('room > users'), function() {
                    kickUser($(this).find('id').text());
                  });
                }, 3000);
                loadTime = new Date() / 1000;
              } else {
                postMessage('ᴿᵉᑫᵘᶦʳᵉᵈ ᶠᵒʳ ᶫᵃᵘᶰᶜʰ﹕20⋅ ʸᵒᵘʳ ʳᵒᶫᶫ﹕' + explode);
              }
            } else {
              postMessage('ᴺᵘᶜᶫᵉᵃʳ ᶜᵒᵈᵉˢ ᶫᵒᶜᵏᵉᵈ⋅');
            }
            break;
          case '/choose':
            //TODO
            break;
          case '/ping':
            postImage('http://i.imgur.com/gMyZGfF.gif#');
            break;
          case '/pong':
            postMessage('ˢᵉᵉ /ping');
            break;
          case '/banner':
            postImage($(roomData).find('room > image').text() + '#');
            break;
          case regexCase(/\/(r|u)\//i, messageWords[0]):
            break;
          default:
            postMessage('ᴵᶰᵛᵃᶫᶦᵈ ᶜᵒᵐᵐᵃᶰᵈ⋅ ᵀʸᵖᵉ /help ᶠᵒʳ ᵐᵒʳᵉ ᶦᶰᶠᵒ⋅');
        }
      } else if (chatMessage.startsWith('-h5-')) {
        postImage('http://i.imgur.com/KMZmCBD.gif#');
      }
      // TODO test this vvv
      // Scans postMessage
      //var resultMention = function(chatMessage, aliases) {
      //  for (var a = 0; a < aliases.length; a++) {
      //    var alias = aliases[a];
      //    if (chatMessage.indexOf(alias) >= 0) {
      //      matchedKeyword = alias;
      //      return true;
      //    }
      //  }
      //  return false;
      //};
      // Scans for mentions of self and creates a desktop notification if found
      //if (resultMention(chatMessage.toUpperCase(), keywords)) {
      //  console.log('detected');
      //  notify(
      //    chatUserName + ' mentioned ' + matchedKeyword.toLowerCase() + '.', //title
      //    chatMessage, //msg
      //    location.origin + '/css/icon/uploads/' + chatIcon + '.png', //icon
      //    scrollToMsg // on click
      //  );
      //}
    }
  };
  var notify = function(nTitle, nMsg, nIcon, nClick, nTime, nFocus) {
    // Default values
    nMsg = typeof nMsg === 'undefined' ? '' : nMsg; //blank
    nTime = typeof nTime === 'undefined' ? 5000 : nTime; //5 seconds. set to false for no timeout
    nIcon = typeof nIcon === 'undefined' ? '' : nIcon; //none
    nClick = typeof nClick === 'undefined' ? scrollToMsg() : nClick; //do nothing
    nFocus = typeof nFocus === 'undefined' ? false : nFocus; //ignore if focused

    if (!Notification) {
      alert('Desktop notifications not available in your browser. Try Chromium.');
      return;
    }
    if (document.hasFocus() && (!nFocus)) {
      return;
    }
    if (Notification.permission !== 'granted') {
      console.log('perms needed');
      Notification.requestPermission();
      return;
    } else {
      var note = new Notification(nTitle, {
        icon: nIcon,
        body: nMsg,
      });
      // Click Action
      note.onclick = nClick;
      // Closes the notification after 5 seconds in chrome
      if (nTime !== false) {
        setTimeout(function() {
          note.close();
        }, nTime);
      }

    }
  };
  var scrollToMsg = function() {
    window.focus();
    console.log('MessageID = ' + $('#' + messageID).offset().top);
    $('html, body').animate({
      scrollTop: $('#' + messageID).offset().top
    }, 300);
  };
  var dice = function(sides, die, raw) {
    var roll;
    var results = [];
    var total = 0;
    var output = '';
    raw = typeof raw !== 'undefined' ? raw : false;
    //Detect and interpret dice notation.
    if (/^\d+d\d+$/i.test(sides)) {
      die = parseInt(sides.match(/^\d+/)[0], 10);
      sides = parseInt(sides.match(/\d+$/)[0], 10);
      //Defualt roll is 1 x 20 sided die.
    } else {
      sides = typeof sides !== 'undefined' ? parseInt(sides, 10) : 20;
      die = typeof die !== 'undefined' ? parseInt(die, 10) : 1;
    }
    //Make sure parameters are valid.

    if (sides !== parseInt(sides, 10) || die !== parseInt(die, 10) || sides < 1 || die < 1) {
      return 'ᴾᶫᵉᵃˢᵉ ᵉᶰᵗᵉʳ ᵖᵒˢᶦᵗᶦᵛᵉ ʷʰᵒᶫᵉ ᶰᵘᵐᵇᵉʳˢ ᶠᵒʳ {sides} ᵃᶰᵈ {dice}, ᵒʳ ᶫᵉᵃᵛᵉ ᵗʰᵒˢᵉ ᵖᵃʳᵃᵐᵉᵗᵉʳˢ ᵇᶫᵃᶰᵏ⋅';
    }
    for (var i = die; i > 0; i--) {
      roll = Math.floor(Math.random() * (sides)) + 1;
      results.push(roll);
      total = total + roll;
    }
    //Output only total if numbers are too big.
    if (raw) {
      return total;
    }
    if (die > 10 || sides > 9999) {
      return 'Total: ' + total;
      //Output only result if only one die.
    } else if (die === 1) {
      return total;
      //Format results.
    } else {
      for (i = 0; i < results.length; i++) {
        output = output + 'd' + (i + 1) + ': ' + results[i] + ' ';
      }
      return output + 'Total: ' + total;
    }
  };
  var populateLog = function(data) {
    $.each($(data).find('talks'), fillLog);
  };
  var fillLog = function() {
    var messageID = $(this).find('id').text();
    scannedIDs.push(messageID);
  };
  // Gets seconds since Epoch at room creation
  var getRoomCreateTime = function(data) {
    roomCreateTime = $(data).find('create').text();
  };
  // Gets total number of messages posted since creation
  var getMessageCount = function(data) {
    messageCount = $(data).find('score').text();
  };
  // Gets the current host's uID
  var getHostID = function(data) {
    hostID = $(data).find('room > host').text();
    return hostID;
  };
  var getHostName = function(data) {
    try {
      hostName = $(data).find("room > users:contains(" + hostID + ") > name").text();
      return hostName;
    } catch (err) {
      console.log('Couldn\'t get name: ' + err.message);
    }
  };
  // Iterates each currently active user's information
  var gatherUserData = function(data) {
    $.each($(data).find('room > users'), scanForData);
  };
  // Uses the host's uID to lookup their user name
  var scanForData = function() {
    var scanningUserID = $(this).find('id').text();
    var scanningUserName = $(this).find('name').text();
    checkForBadChars(scanningUserID, scanningUserName);
    slayEnemies(scanningUserID, scanningUserName);
    trackedUsers[scanningUserName.toUpperCase()] = new Date() / 1000;
  };
  var slayEnemies = function(sinnerID, sinner) {
    //fuck xeph
    if ($.inArray(sinner.toUpperCase(), theSlain) !== -1 || /lurk/i.test(sinner)) {
      $.post(postURL, {
        'ban_user': sinnerID,
        'block': 1
      });
    }
  };
  var checkForBadChars = function(id, name) {
    var blLength = blacklistedChars.length;
    if (/^\s+|\s+$/g.test(name)) {
      console.log('space at edge');
      kickUser(id, 'ᵇᵃᵈ ᶜʰᵃʳᵃᶜᵗᵉʳ ᶦᶰ ᵘˢᵉʳᶰᵃᵐᵉ "' + name + '"');
      return;
    }
    for (var i = 0; i < blLength; i++) {
      if (name.indexOf(blacklistedChars[i]) !== -1) {
        console.log('bad char detect');
        kickUser(id, 'ᵇᵃᵈ ᶜʰᵃʳᵃᶜᵗᵉʳ ᶦᶰ ᵘˢᵉʳᶰᵃᵐᵉ "' + name + '"');
        return;
      }
    }
  };
  var kickReason = function(reason) { //to the curb
    if (new Date() / 1000 - warnTime > 180) { //time since last message > 3 mins
      postMessage(reason);
      warnTime = new Date() / 1000;
    }
  };
  var getIconFromName = function(name) {
    try {
      return $(roomData).find("room > users:contains(" + name + ") > icon").text();
    } catch (err) {
      console.log('Couldn\'t get icon: ' + err.message);
    }
  };
  var getID = function(user) {
    seenSearch = user;
    $.each($(roomData).find('room > users'), findID);
    return searchID;
  };
  var findID = function() {
    var scanningUserID = $(this).find('id').text();
    var scanningUserName = $(this).find('name').text();
    if (seenSearch == scanningUserName) {
      searchID = scanningUserID;
    }
  };
  //Iterates through each online user
  var iterateOnlineUsers = function() {
    userOnline = false;
    $.each($(roomData).find('room > users'), checkOnlineUsers);
    if (userOnline) {
      userOnline = false;
      return true;
    } else {
      return false;
    }
  };
  //Checks if current username matches query
  var checkOnlineUsers = function() {
    var scanningUserName = $(this).find('name').text().toUpperCase();
    if (seenSearch.toUpperCase() == scanningUserName) {
      console.log('User matched');
      userOnline = true;
    }
  };
  // Returns the input string if the regex matches. For swiches.
  var regexCase = function(regex, strMatch) {
    if (regex.test(strMatch)) {
      return strMatch.toLowerCase();
    }
  };
  //Takes a date in seconds, and outputs time since epoch in formatted manner
  var displayTime = function(seconds, future) {
    var now = new Date() / 1000;
	var s
    if (future) {
      s = seconds - now;
    } else {
      s = now - seconds;
    }
    var d = Math.floor(s / 86400); // Get whole days
    s -= d * 86400;
    var h = Math.floor(s / 3600); //Get whole hours
    s -= h * 3600;
    var m = Math.floor(s / 60); //Get remaining minutes
    s -= m * 60;
    //If the largest unit is days, don't display minutes or seconds.
    if (d > 0) {
      return (d == 1 ? d + ' ᵈᵃʸ ' : d + ' ᵈᵃʸˢ ') + (h === 0 ? '' : h == 1 ? 'ᵃᶰᵈ ' + h + ' ʰᵒᵘʳ ' : 'ᵃᶰᵈ ' + h + ' ʰᵒᵘʳˢ ');
      //If the largest unit is hours, don't display seconds.
    } else if (h > 0) {
      return (h == 1 ? h + ' ʰᵒᵘʳ ' : h + ' ʰᵒᵘʳˢ ') + (m === 0 ? '' : m == 1 ? 'ᵃᶰᵈ 0' + m + ' ᵐᶦᶰᵘᵗᵉ ' : m < 10 ? 'ᵃᶰᵈ 0' + m + ' ᵐᶦᶰᵘᵗᵉˢ ' : 'ᵃᶰᵈ ' + m + ' ᵐᶦᶰᵘᵗᵉˢ ');
    } else if (m > 0) {
      return (m == 1 ? '0' + m + ' ᵐᶦᶰᵘᵗᵉ ' : m < 10 ? '0' + m + ' ᵐᶦᶰᵘᵗᵉˢ ' : m + ' ᵐᶦᶰᵘᵗᵉˢ ') + (s === 0 ? '0' + Math.round(s) : s == 1 ? '0' + Math.round(s) + ' ˢᵉᶜᵒᶰᵈ ' : s < 10 ? '0' + Math.round(s) + ' ˢᵉᶜᵒᶰᵈˢ ' : Math.round(s) + ' ˢᵉᶜᵒᶰᵈˢ ');
    } else {
      return (s === 0 ? 'ᵃᶰ ᶦᶰˢᵗᵃᶰᵗ ' : s == 1 ? '0' + Math.round(s) + ' ˢᵉᶜᵒᶰᵈ ' : s < 10 ? '0' + Math.round(s) + ' ˢᵉᶜᵒᶰᵈˢ ' : Math.round(s) + ' ˢᵉᶜᵒᶰᵈˢ ');
    }
  };
  if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function(str) {
      return this.indexOf(str) === 0;
    };
  }
  //Kicks user by ID passed
  var kickUser = function(userID, cbParam) {
    if (typeof cbParam !== 'string') cbParam = '';
    if (userID === selfID) {
      return;
    }
    if (hostID !== selfID) {
      if (new Date() / 1000 - askHostTime > 600) {
        if (hostID == userID) {
          postMessage('ᶜ⁻ᶜᵃᶰ ʸᵒᵘ ᶫᵉᵗ ᵐᵉ ᵏᶦᶜᵏ ʸᵒᵘ, @' + hostName + '﹖ ⁽´・ω・`⁾');
          askHostTime = new Date() / 1000;
          return;
        }
        postMessage('@' + hostName + ', ᵖᶫᵉᵃˢᵉ ʳᵉᵗᵘʳᶰ ʰᵒˢᵗ ᵗᵒ ᵐᵉ ᵗᵒ ᵃᶫᶫᵒʷ ᵃᵘᵗᵒ⁻ᵐᵒᵈᵉʳᵃᵗᶦᵒᶰ⋅ ' + cbParam);
        askHostTime = new Date() / 1000;
      }
    } else if (hostID === selfID) {
      $.post(postURL, {
        'ban_user': userID,
        'block': 1
      }, kickReason(cbParam));
    }
  };
  // Begin main function
  initialize();
});
